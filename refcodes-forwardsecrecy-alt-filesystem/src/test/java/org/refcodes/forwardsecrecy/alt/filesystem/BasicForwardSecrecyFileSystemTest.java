// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy.alt.filesystem;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.net.URI;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.refcodes.factory.BeanLookupFactory;
import org.refcodes.factory.alt.spring.SpringBeanFactory;
import org.refcodes.filesystem.ConcurrentAccessException;
import org.refcodes.filesystem.FileSystem;
import org.refcodes.filesystem.FileSystemUtility;
import org.refcodes.filesystem.IllegalKeyException;
import org.refcodes.filesystem.IllegalPathException;
import org.refcodes.filesystem.NoDeleteAccessException;
import org.refcodes.filesystem.NoListAccessException;
import org.refcodes.filesystem.UnknownFileException;
import org.refcodes.filesystem.UnknownFileSystemException;
import org.refcodes.filesystem.UnknownPathException;
import org.refcodes.forwardsecrecy.AbstractDecryptionService;
import org.refcodes.forwardsecrecy.CipherUidAlreadyInUseException;
import org.refcodes.forwardsecrecy.DecryptionProvider;
import org.refcodes.forwardsecrecy.EncryptionProvider;
import org.refcodes.forwardsecrecy.NoCipherUidException;
import org.refcodes.forwardsecrecy.UnknownCipherUidException;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.security.EncryptionException;
import org.springframework.core.io.ClassPathResource;

public class BasicForwardSecrecyFileSystemTest {

	private static final String FORWARD_SECRECY_PATH = "refcodes/cryptography";
	private static final String NAMESPACE = "test";
	private static final String DATA_RECORD_PATH = FORWARD_SECRECY_PATH + "/" + NAMESPACE;
	private static BeanLookupFactory<String> lookupFactory;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// LIFECYCLE:

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@BeforeAll
	public static void beforeAll() throws IOException, NoDeleteAccessException, IllegalKeyException, NoListAccessException, UnknownFileSystemException, UnknownFileException, ConcurrentAccessException, IllegalPathException, UnknownPathException {
		lookupFactory = new SpringBeanFactory( new URI[] { new ClassPathResource( "basic-refcodes-forwardsecrecy-context.xml" ).getURI() }, new URI[] { new ClassPathResource( "basic-refcodes-forwardsecrecy.conf" ).getURI() } );
		cleanUp();
	}

	@AfterAll
	public static void afterAll() throws NoListAccessException, IllegalKeyException, UnknownFileSystemException, IOException, NoDeleteAccessException, ConcurrentAccessException, UnknownFileException, IllegalPathException, UnknownPathException {
		cleanUp();
	}

	private static void cleanUp() throws IllegalKeyException, NoListAccessException, UnknownFileSystemException, IOException, UnknownFileException, ConcurrentAccessException, NoDeleteAccessException, IllegalPathException, UnknownPathException {
		final FileSystem theFileSystem = lookupFactory.create( "forwardSecrecyFileSystem" );
		FileSystemUtility.deleteFiles( theFileSystem, DATA_RECORD_PATH, true );
	}

	@Test
	public void testDecryptionServerPublicKeyWrapperImpl() throws IOException, CipherUidAlreadyInUseException, UnknownFileException, NoListAccessException, IllegalKeyException, UnknownFileSystemException, NoDeleteAccessException, ConcurrentAccessException, IllegalPathException, UnknownPathException, UnknownCipherUidException, NoCipherUidException, EncryptionException {
		final EncryptionProvider theEncryptionProvider = lookupFactory.create( "encryptionProvider" );
		final DecryptionProvider theDecryptionProvider = lookupFactory.create( "decryptionProvider" );
		theEncryptionProvider.nextCipherVersion();
		final String theText = "Hallo Welt!";
		String theEncryptedText = theEncryptionProvider.toEncrypted( theText );
		String theDecryptedText = theDecryptionProvider.toDecrypted( theEncryptedText );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Text = " + theText );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Encrypted text = " + theEncryptedText );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Decrypted text = " + theDecryptedText );
		}
		assertEquals( theText, theDecryptedText );
		// -----------
		// Speed test:
		// -----------
		final long theStartTime = System.currentTimeMillis();
		final int theRuns = 1000;
		// ---------------------------------------------------------------------
		// Shortcut for forcing reloading ciphers after 1000 ms expiration time:
		// ---------------------------------------------------------------------
		final AbstractDecryptionService theDecryptionService = lookupFactory.create( "decryptionService" );
		theDecryptionService.setCipherVersionsExpireTimeMillis( 1000 );
		// ---------------------------------------------------------------------
		for ( int i = 0; i < theRuns; i++ ) {
			if ( i % 100 == 0 ) {
				theEncryptionProvider.nextCipherVersion();
			}
			theEncryptedText = theEncryptionProvider.toEncrypted( theText + i );
			theDecryptedText = theDecryptionProvider.toDecrypted( theEncryptedText );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( theDecryptedText + " --> " + theEncryptedText );
			}
			assertEquals( theText + i, theDecryptedText );
		}
		final long theEndTime = System.currentTimeMillis();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "ASYMETRIC ENCRYPTION:" );
		}
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Milliseconds/run = " + ( ( (double) theEndTime - (double) theStartTime ) / theRuns ) );
		}
	}
}
