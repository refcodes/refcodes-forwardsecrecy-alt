module org.refcodes.forwardsecrecy.alt.filesystem {
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.filesystem;
	requires transitive org.refcodes.forwardsecrecy;
	requires java.logging;
	requires org.refcodes.textual;
	requires org.refcodes.runtime;

	exports org.refcodes.forwardsecrecy.alt.filesystem;
}
