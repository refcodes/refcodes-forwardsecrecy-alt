// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy.alt.filesystem;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.exception.Trap;
import org.refcodes.filesystem.ConcurrentAccessException;
import org.refcodes.filesystem.FileHandle;
import org.refcodes.filesystem.FileSystem;
import org.refcodes.filesystem.FileSystemUtility;
import org.refcodes.filesystem.IllegalFileException;
import org.refcodes.filesystem.IllegalPathException;
import org.refcodes.filesystem.NoListAccessException;
import org.refcodes.filesystem.NoReadAccessException;
import org.refcodes.filesystem.UnknownFileException;
import org.refcodes.filesystem.UnknownFileSystemException;
import org.refcodes.filesystem.UnknownPathException;
import org.refcodes.forwardsecrecy.CipherVersion;
import org.refcodes.forwardsecrecy.DecryptionServer;
import org.refcodes.forwardsecrecy.SignatureVerificationException;
import org.refcodes.textual.SecretHintBuilder;

/**
 * Abstract file system based implementation for non abstract
 * {@link DecryptionServer} implementations.
 */
public class FileSystemDecryptionServer implements DecryptionServer {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( FileSystemDecryptionServer.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final FileSystem _fileSystem;

	private final String _path;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the server with the required services and configuration
	 * passed.
	 * 
	 * @param aFileSystem The data store service where to retrieve the cipher
	 *        versions from
	 * @param aPath The directory where the cryptography relevant data is being
	 *        persisted
	 */
	public FileSystemDecryptionServer( FileSystem aFileSystem, String aPath ) {
		_fileSystem = aFileSystem;
		_path = FileSystemUtility.toNormalizedPath( aPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CipherVersion> getCipherVersions( String aNameSpace, String aMessage, String aSignature ) throws SignatureVerificationException {

		final String theNameSpaceKey = FileSystemUtility.toKey( _path, aNameSpace );
		LOGGER.log( Level.FINE, "Using file system key \"" + theNameSpaceKey + "\"." );

		try {
			final List<FileHandle> theFileHandles = _fileSystem.getFileHandles( theNameSpaceKey, true );
			ObjectInputStream eObjectInputStream = null;
			InputStream eInputStream = null;
			CipherVersion eCipherVersion;
			final List<CipherVersion> theEncryptedCipherVersions = new ArrayList<>();
			for ( FileHandle eFileHandle : theFileHandles ) {
				LOGGER.log( Level.FINE, "Trying to load cipher version from file (handle) with key \"" + eFileHandle.toKey() + "\" ..." );
				try {
					eInputStream = _fileSystem.fromFile( eFileHandle );
					eObjectInputStream = new ObjectInputStream( eInputStream );
					eCipherVersion = (CipherVersion) eObjectInputStream.readObject();
					theEncryptedCipherVersions.add( eCipherVersion );
					LOGGER.log( Level.FINE, "Loaded cipher version from file (handle) with key \"" + eFileHandle.toKey() + "\"!" );
				}
				finally {
					if ( eObjectInputStream != null ) {
						try {
							eObjectInputStream.close();
						}
						catch ( IOException e ) {
							LOGGER.log( Level.WARNING, "Unable to close output stream for file system path \"" + eFileHandle.getPath() + "\" and name \"" + eFileHandle.getName() + "\" as of:" + Trap.asMessage( e ), e );
						}
					}
					if ( eInputStream != null ) {
						try {
							eInputStream.close();
						}
						catch ( IOException e ) {
							LOGGER.log( Level.WARNING, "Unable to close object output stream for file system path \"" + eFileHandle.getPath() + "\" and name \"" + eFileHandle.getName() + "\" as of: " + Trap.asMessage( e ), e );
						}
					}
				}
			}

			return theEncryptedCipherVersions;
		}
		// catch ( Exception e ) {
		// 	if ( e instanceof SignatureVerificationException) throw (SignatureVerificationException) e;
		// }
		catch ( ClassNotFoundException | NoReadAccessException | ConcurrentAccessException | UnknownFileException | IllegalFileException | NoListAccessException | UnknownPathException | IllegalPathException | UnknownFileSystemException | IOException e ) {
			throw new IllegalArgumentException( "Cannot get cipher version for signature <" + SecretHintBuilder.asString( aSignature ) + "> and message <" + SecretHintBuilder.asString( aMessage ) + " in name space <" + aNameSpace + ">!", e );
		}
	}
}
