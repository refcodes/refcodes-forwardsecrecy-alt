// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.forwardsecrecy.alt.filesystem;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.Component;
import org.refcodes.data.FilenameExtension;
import org.refcodes.exception.Trap;
import org.refcodes.filesystem.ConcurrentAccessException;
import org.refcodes.filesystem.FileAlreadyExistsException;
import org.refcodes.filesystem.FileHandle;
import org.refcodes.filesystem.FileSystem;
import org.refcodes.filesystem.FileSystemUtility;
import org.refcodes.filesystem.IllegalFileException;
import org.refcodes.filesystem.IllegalNameException;
import org.refcodes.filesystem.IllegalPathException;
import org.refcodes.filesystem.NoCreateAccessException;
import org.refcodes.filesystem.NoListAccessException;
import org.refcodes.filesystem.NoWriteAccessException;
import org.refcodes.filesystem.UnknownFileException;
import org.refcodes.filesystem.UnknownFileSystemException;
import org.refcodes.filesystem.UnknownPathException;
import org.refcodes.forwardsecrecy.CipherUidAlreadyInUseException;
import org.refcodes.forwardsecrecy.CipherVersion;
import org.refcodes.forwardsecrecy.EncryptionServer;
import org.refcodes.textual.SecretHintBuilder;

/**
 * Abstract file system based implementation for non abstract
 * {@link EncryptionServer} implementations.
 */
public class FileSystemEncryptionServer implements EncryptionServer, Component {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( FileSystemEncryptionServer.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private FileSystem _fileSystem;

	private final String _path;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the server with the required services and configuration
	 * passed.
	 * 
	 * @param aFileSystem The data store service where to retrieve the cipher
	 *        versions from
	 * @param aDataStorePath The directory where the cryptography relevant data
	 *        is being persisted
	 */
	public FileSystemEncryptionServer( FileSystem aFileSystem, String aDataStorePath ) {
		_fileSystem = aFileSystem;
		_path = FileSystemUtility.toNormalizedPath( aDataStorePath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addCipherVersion( String aNamespace, CipherVersion aCipherVersion ) throws CipherUidAlreadyInUseException, IOException {

		final String theNameSpaceKey = FileSystemUtility.toKey( _path, aNamespace );
		LOGGER.log( Level.FINE, "Using file system key <" + theNameSpaceKey + ">." );

		OutputStream theOutputStream = null;
		ObjectOutputStream theObjectOutputStream = null;

		try {
			final List<FileHandle> theFileHandles = _fileSystem.getFileHandles( theNameSpaceKey, false );
			String eName;
			String eCipherUid;
			for ( FileHandle eFile : theFileHandles ) {
				eName = eFile.getName();
				if ( eName.endsWith( FilenameExtension.CIPHER_VERSION.getFilenameSuffix() ) ) {
					eCipherUid = eName.substring( 0, ( eName.length() - FilenameExtension.CIPHER_VERSION.getFilenameSuffix().length() ) );
					LOGGER.log( Level.FINE, "Found file <" + eName + "> containing cipher version for cipher UID <" + eCipherUid + ">." );
					if ( eCipherUid.equals( aCipherVersion.getUniversalId() ) ) {
						throw new CipherUidAlreadyInUseException( "The cipher UID <{1}> of namespace <{0}> is already in use by another cipher version, aborting adding a new cipher version! ", aNamespace, aCipherVersion.getUniversalId() );
					}
				}
			}

			final FileHandle theFile = _fileSystem.createFile( theNameSpaceKey, aCipherVersion.getUniversalId() + FilenameExtension.CIPHER_VERSION.getFilenameSuffix() );

			theOutputStream = new ByteArrayOutputStream();
			theObjectOutputStream = new ObjectOutputStream( theOutputStream );
			theObjectOutputStream.writeObject( aCipherVersion );
			theObjectOutputStream.flush();
			final InputStream theInputStream = new ByteArrayInputStream( ( (ByteArrayOutputStream) theOutputStream ).toByteArray() );
			_fileSystem.toFile( theFile, theInputStream );
		}
		catch ( FileAlreadyExistsException | ConcurrentAccessException e ) {
			throw new CipherUidAlreadyInUseException( "The cipher UID <{1}> of namespace <{0}> is already in use by another cipher version, aborting adding a new cipher version!", aNamespace, aCipherVersion.getUniversalId(), e );
		}
		catch ( NoListAccessException | UnknownPathException | IllegalPathException | IllegalFileException | UnknownFileSystemException | NoCreateAccessException | IllegalNameException | NoWriteAccessException | UnknownFileException e ) {
			throw new IOException( "Cannot add cipher with UID <" + aCipherVersion.getUniversalId() + "> and cipher <" + SecretHintBuilder.asString( aCipherVersion.getCipher() ) + "> of name space <" + aNamespace + ">!", e );
		}
		finally {
			if ( theObjectOutputStream != null ) {
				try {
					theObjectOutputStream.close();
				}
				catch ( IOException e ) {
					LOGGER.log( Level.WARNING, "Unable to close output stream for file system path <" + _path + "> and name <" + aCipherVersion.getUniversalId() + FilenameExtension.CIPHER_VERSION.getFilenameSuffix() + ">!", e );
				}
			}
			if ( theOutputStream != null ) {
				try {
					theOutputStream.close();
				}
				catch ( IOException e ) {
					LOGGER.log( Level.WARNING, "Unable to close object output stream for file system path <" + _path + "> and name <" + aCipherVersion.getUniversalId() + FilenameExtension.CIPHER_VERSION.getFilenameSuffix() + "> as of: " + Trap.asMessage( e ), e );
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// SERVICE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		final FileSystem theFileSystem = _fileSystem;
		if ( theFileSystem != null ) {
			theFileSystem.destroy();
		}
		_fileSystem = null;
	}
}
